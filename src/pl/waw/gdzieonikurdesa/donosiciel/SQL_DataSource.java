package pl.waw.gdzieonikurdesa.donosiciel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class SQL_DataSource {

	// Database fields
	private SQLiteDatabase database;
	private SQL_Helper dbHelper;
	private String[] allColumns = { SQL_Helper.COLUMN_ID,
			SQL_Helper.COLUMN_ACCURACY, SQL_Helper.COLUMN_ALTITUDE,
			SQL_Helper.COLUMN_BEARING, SQL_Helper.COLUMN_LATITUDE,
			SQL_Helper.COLUMN_LONGITUDE, SQL_Helper.COLUMN_PROVIDER,
			SQL_Helper.COLUMN_SPEED, SQL_Helper.COLUMN_TIMESTAMP };

	public SQL_DataSource(Context context) {
		dbHelper = new SQL_Helper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public SQL_ModelGPS createSample(long timeStamp, double latitude,
			double longitude, double altitude, float speed,
			float accuracy, float bearing, String provider) {

		ContentValues values = new ContentValues();

		values.put(SQL_Helper.COLUMN_TIMESTAMP, timeStamp);
		values.put(SQL_Helper.COLUMN_LATITUDE, latitude);
		values.put(SQL_Helper.COLUMN_LONGITUDE, longitude);
		values.put(SQL_Helper.COLUMN_ALTITUDE, altitude);
		values.put(SQL_Helper.COLUMN_SPEED, speed);
		values.put(SQL_Helper.COLUMN_ACCURACY, accuracy);
		values.put(SQL_Helper.COLUMN_BEARING, bearing);
		values.put(SQL_Helper.COLUMN_PROVIDER, provider);

		long insertId = database.insert(SQL_Helper.TABLE_GPS, null, values);
		Cursor cursor = database
				.query(SQL_Helper.TABLE_GPS, allColumns, SQL_Helper.COLUMN_ID
						+ " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		SQL_ModelGPS newSample = cursorToSample(cursor);
		cursor.close();
		return newSample;
	}

	public List<HashMap<String, String>> getNotSyncedSamples(String lastTimeStamp) {
	    List<HashMap<String, String>> samples = new ArrayList<HashMap<String, String>>();

	    Cursor cursor = database.query(SQL_Helper.TABLE_GPS,
	        allColumns, SQL_Helper.COLUMN_TIMESTAMP + ">" + lastTimeStamp, null, null, null, SQL_Helper.COLUMN_TIMESTAMP, "0,10000");
	    
	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      SQL_ModelGPS sample = cursorToSample(cursor);
	      HashMap<String, String> temp = new HashMap<String, String>();
	      temp.put(SQL_Helper.COLUMN_TIMESTAMP, String.valueOf(sample.getTimeStamp()));
	      temp.put(SQL_Helper.COLUMN_LATITUDE, String.valueOf(sample.getLatitude()));
	      temp.put(SQL_Helper.COLUMN_LONGITUDE, String.valueOf(sample.getLongitude()));
	      temp.put(SQL_Helper.COLUMN_ALTITUDE, String.valueOf(sample.getAltitude()));
	      temp.put(SQL_Helper.COLUMN_SPEED, String.valueOf(sample.getSpeed()));
	      temp.put(SQL_Helper.COLUMN_ACCURACY, String.valueOf(sample.getAccuracy()));
	      temp.put(SQL_Helper.COLUMN_BEARING, String.valueOf(sample.getBearing()));
	      temp.put(SQL_Helper.COLUMN_PROVIDER, sample.getProvider());
	      samples.add(temp);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return samples;
	  }
	
	public void deleteSample(SQL_ModelGPS sample) {
		long id = sample.getId();
		database.delete(SQL_Helper.TABLE_GPS,
				SQL_Helper.COLUMN_ID + " = " + id, null);
	}
	
	public SQL_ModelGPS getLast() {
		Cursor cursor = database.query(SQL_Helper.TABLE_GPS, allColumns, null, null, null, null, "f_timestamp DESC", "0, 1");
		SQL_ModelGPS sample = cursorToSample(cursor);
		cursor.close();
		return sample;
	}

	private SQL_ModelGPS cursorToSample(Cursor cursor) {
		SQL_ModelGPS sample = new SQL_ModelGPS();
		sample.setId(cursor.getLong(cursor.getColumnIndex(SQL_Helper.COLUMN_ID)));
		sample.setTimeStamp(cursor.getLong(cursor.getColumnIndex(SQL_Helper.COLUMN_TIMESTAMP)));
		sample.setLatitude(cursor.getDouble(cursor.getColumnIndex(SQL_Helper.COLUMN_LATITUDE)));
		sample.setLongitude(cursor.getDouble(cursor.getColumnIndex(SQL_Helper.COLUMN_LONGITUDE)));
		sample.setAltitude(cursor.getDouble(cursor.getColumnIndex(SQL_Helper.COLUMN_ALTITUDE)));
		sample.setSpeed(cursor.getFloat(cursor.getColumnIndex(SQL_Helper.COLUMN_SPEED)));
		sample.setAccuracy(cursor.getFloat(cursor.getColumnIndex(SQL_Helper.COLUMN_ACCURACY)));
		sample.setBearing(cursor.getFloat(cursor.getColumnIndex(SQL_Helper.COLUMN_BEARING)));
		sample.setProvider(cursor.getString(cursor.getColumnIndex(SQL_Helper.COLUMN_PROVIDER)));		
		return sample;
	}
}
