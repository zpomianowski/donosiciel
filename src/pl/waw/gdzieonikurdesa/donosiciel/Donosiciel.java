package pl.waw.gdzieonikurdesa.donosiciel;

import pl.waw.gdzieonikurdesa.donosiciel.R;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.ch;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Donosiciel extends Activity {
	public static final String PREFS_NAME = "DonosicielSettings";
	private static final int DESC_FAIL_SAVE_PERIOD = 30000; // 30s
	private static final int DESC_FAIL_SYNC_PERIOD = 43200000; // 12h

	final int RQS_GooglePlayServices = 1;
	ToggleButton tbtn_service;
	EditText editText_email;
	EditText editText_passw;
	
	Button button_save;
	CheckBox checkBox_failure;
	EditText editText_desc;
	String lastDesc = null;
	long synced = -1;

	private Boolean mIsBound = false;
	@SuppressWarnings("unused")
	private DonosicielService mService;
	private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
		public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((DonosicielService.LocalBinder)service).getService();
        }

        @Override
		public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };
    
    void doBindService() {
    	Intent i = new Intent(Donosiciel.this, DonosicielService.class);
    	i.setAction("CONFIG");
        bindService(i, mServiceConnection, Context.BIND_AUTO_CREATE);	
        mIsBound = true;
    }
    
    void doUnbindService() {
        if (mIsBound) {
            unbindService(mServiceConnection);
            mIsBound = false;
        }
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      
		tbtn_service = (ToggleButton) findViewById(R.id.toggleService);
		editText_email = (EditText) findViewById(R.id.editText_email);
		editText_passw = (EditText) findViewById(R.id.editText_passw);
		
		editText_desc = (EditText) findViewById(R.id.editText_desc);
		checkBox_failure = (CheckBox) findViewById(R.id.checkBox_failure);
		button_save = (Button) findViewById(R.id.button_save);
		
		if (savedInstanceState != null) {
			tbtn_service.setChecked(savedInstanceState.getBoolean("service"));
			editText_email.setText(savedInstanceState.getString("email"));
			editText_passw.setText(savedInstanceState.getString("passw"));
			
			synced = savedInstanceState.getInt("synced");
			editText_desc.setText(lastDesc = savedInstanceState.getString("desc"));
			checkBox_failure.setChecked(savedInstanceState.getBoolean("failure"));
		} else {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			
			// Service control
			Boolean serviceIsActive = settings.getBoolean("service", false);
			editText_email.setText(settings.getString("email", "uzytkownik@domena.pl"));
			editText_passw.setText(settings.getString("passw", "has�o"));
			
			tbtn_service.setChecked(serviceIsActive);
			if (serviceIsActive) doBindService();
			
			// Desc and Failure
			synced = settings.getLong("synced", -1L);
			editText_desc.setText(lastDesc = settings.getString("desc", ""));
			checkBox_failure.setChecked(settings.getBoolean("failure", false));
		}
    }
    
    @Override
    public void onResume() {
    	super.onResume();
		// get desc fail update
		getDescFail();

    	int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
    	if (resultCode == ConnectionResult.SERVICE_MISSING ||
    	           resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
    	           resultCode == ConnectionResult.SERVICE_DISABLED) {
    	    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
    	    dialog.show();
    	}
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
	    savedInstanceState.putBoolean("service",
	    		((ToggleButton)findViewById(R.id.toggleService)).isChecked());
	    savedInstanceState.putLong("synced", synced);
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
    	super.onRestoreInstanceState(savedInstanceState);
    	((ToggleButton)findViewById(R.id.toggleService))
    		.setChecked(savedInstanceState.getBoolean("service"));
    	synced = savedInstanceState.getLong("synced");
    }
    
    public void onToggleServiceClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        
        if (on) {
        	Intent i = new Intent(Donosiciel.this, DonosicielService.class);
        	i.putExtra("USER_NAME", editText_email.getText().toString());
        	i.putExtra("PASSWORD", editText_passw.getText().toString());
        	i.setAction("CONFIG");
        	startService(i);
        	doBindService();
        } else {
        	doUnbindService();
        	stopService(new Intent(Donosiciel.this, DonosicielService.class));
        }
    }
        
    @Override
    protected void onStop(){
      super.onStop();
      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
      SharedPreferences.Editor editor = settings.edit();
      editor.putBoolean("service", tbtn_service.isChecked());
      editor.putString("email", editText_email.getText().toString());
      editor.putString("passw", editText_passw.getText().toString());
      editor.putLong("synced", synced);
      editor.putString("desc", editText_desc.getText().toString());
      editor.putBoolean("failure", checkBox_failure.isChecked());
      
      editor.commit();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
        doUnbindService();
	}

    private void getDescFail(){
    	if ((System.currentTimeMillis() - synced) < DESC_FAIL_SYNC_PERIOD)
    		return;

    	new DescFailTask(this, editText_email.getText().toString(), editText_passw.getText().toString())
    		.execute("GET");
    }

    public void onSaveClicked(View v){
    	if ((System.currentTimeMillis() - synced) < DESC_FAIL_SAVE_PERIOD){
    		Toast.makeText(this, R.string.df_slowdown, Toast.LENGTH_LONG).show();
    		return;
    	}

    	new DescFailTask(this, editText_email.getText().toString(), editText_passw.getText().toString())
			.execute("SET", editText_desc.getText().toString(), checkBox_failure.isChecked() ? "true" : "false");
    }
    
    public void setDescFail(String desc, Boolean failure){
    	setSync();
    	editText_desc.setText(lastDesc = desc);
    	checkBox_failure.setChecked(failure);
    }
    
    public void setSync(){
    	synced = System.currentTimeMillis();
    }
}