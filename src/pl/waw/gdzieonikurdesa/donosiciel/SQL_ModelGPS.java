package pl.waw.gdzieonikurdesa.donosiciel;

public class SQL_ModelGPS {
	private long id;
	private long f_timeStamp;
	private double f_latitude;
	private double f_longitude;
	private double f_altitude;
	private float f_speed;
	private float f_accuracy;
	private float f_bearing;
	private String f_provider;

	public long getId() { return id; }
	public void setId(long id) { this.id = id; }
	
	public long getTimeStamp() { return f_timeStamp; }
	public void setTimeStamp(long timeStamp) { this.f_timeStamp = timeStamp; }
	
	public double getLatitude() { return f_latitude; }
	public void setLatitude(double f_latitude) { this.f_latitude = f_latitude; }
	
	public double getLongitude() { return f_longitude; }
	public void setLongitude(double f_longitude) { this.f_longitude = f_longitude; }
	
	public double getAltitude() { return f_altitude; }
	public void setAltitude(double f_altitude) { this.f_altitude = f_altitude; }
	
	public float getSpeed() { return f_speed; }
	public void setSpeed(float f_speed) { this.f_speed = f_speed; }
	
	public float getAccuracy() { return f_accuracy; }
	public void setAccuracy(float f_accuracy) { this.f_accuracy = f_accuracy; }
	
	public float getBearing() { return f_bearing; }
	public void setBearing(float f_bearing) { this.f_bearing = f_bearing; }
	
	public String getProvider() { return f_provider; }
	public void setProvider(String f_provider) { this.f_provider = f_provider; }
}
